A PoC of model gateway.

## Usage
It is vey simple to test and play with this PoC.

Currently supported models are:
- gpt-4
- claude-3-haiku
- claude-3-sonnet
- claude-3-opus

1. Set `OPENAI_TOKEN` and `ANTHROPIC_TOKEN` environment variables.
1. Start the server by running `cargo run`. The server will listen at `localhost:3000`.
1. Send post requests to call any supported LLMs:
    ```shell
    curl -X POST --header "content-type: application/json" --data '{ "model": "gpt-4", "user_prompt": "Write a short poem about rust the programming language", "system_prompt": "", "start_of_sequence": "" }' localhost:3000
    ```
