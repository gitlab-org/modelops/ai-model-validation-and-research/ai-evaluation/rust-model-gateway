use axum::{routing, Json, Router};
use serde::{Deserialize, Serialize};
use serde_json::Value;

use rust_model_gateway::ModelName;

#[tokio::main]
async fn main() {
    let app = Router::new()
        .route("/", routing::post(route_llm))
        .route("/greet", routing::get(|| async { "Hello!" }));

    // run our app with hyper, listening globally on port 3000
    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(listener, app).await.unwrap();
}

#[derive(Deserialize, Serialize)]
struct LLMRequest {
    model: ModelName,
    user_prompt: String,
    system_prompt: String,
    start_of_sequence: String,
    parameters: Option<Value>,
}

async fn route_llm(Json(payload): Json<LLMRequest>) -> String {
    let model_handle = rust_model_gateway::model_gateway::get_model_handle(payload.model);
    let a = model_handle
        .get_response(&payload.user_prompt, payload.parameters)
        .await;
    // let a = serde_json::to_string(&payload).unwrap();
    a.expect("Something bad happened")
}
