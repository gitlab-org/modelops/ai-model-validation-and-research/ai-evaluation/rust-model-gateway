mod anthropic_models;
mod openai_models;

use std::env;
use anthropic_models::AnthropicModelHandle;
use openai_models::OpenAIModelHandle;
use serde::{Deserialize, Serialize};

#[derive(Debug)]
pub enum APIError {
    ParseError(serde_json::Error),
    HTTPError(String),
    RequestError(reqwest::Error),
}

impl From<reqwest::Error> for APIError {
    fn from(e: reqwest::Error) -> Self {
        Self::RequestError(e)
    }
}

impl From<serde_json::Error> for APIError {
    fn from(e: serde_json::Error) -> Self {
        Self::ParseError(e)
    }
}

#[derive(Serialize, Deserialize)]
pub enum ModelName {
    #[serde(rename = "gpt-4")]
    Gpt4,

    #[serde(rename = "claude-3-haiku")]
    Claude3Haiku,

    #[serde(rename = "claude-3-sonnet")]
    Claude3Sonnet,

    #[serde(rename = "claude-3-opus")]
    Claude3Opus,
}

pub enum ModelHandle {
    OPENAI(OpenAIModelHandle),
    ANTHROPIC(AnthropicModelHandle),
}

impl ModelHandle {
    pub async fn get_response(
        &self,
        prompt: &str,
        parameters: Option<serde_json::Value>,
    ) -> Result<String, APIError> {
        match self {
            ModelHandle::OPENAI(handle) => handle.get_response(prompt, parameters).await,
            ModelHandle::ANTHROPIC(handle) => handle.get_response(prompt, parameters).await,
        }
    }
}

const ANTHROPIC_URL: &str = "https://api.anthropic.com/v1/messages";
const OPENAI_URL: &str = "https://api.openai.com/v1/chat/completions";

pub fn get_model_handle(model_name: ModelName) -> ModelHandle {
    match model_name {
        ModelName::Gpt4 => {
            let openai_token = env::var("OPENAI_TOKEN").expect("Cannot find openai token. Please set OPENAI_TOKEN env variable");
            ModelHandle::OPENAI(OpenAIModelHandle {
                base_url: String::from(OPENAI_URL),
                model_name: String::from("gpt-4"),
                api_token: openai_token,
                parameters: None,
            })
        },
        ModelName::Claude3Haiku => {
            let anthropic_token = env::var("ANTHROPIC_TOKEN").expect("Cannot find anthropic token. Please set ANTHROPIC_TOKEN env variable");
            ModelHandle::ANTHROPIC(AnthropicModelHandle {
                base_url: String::from(ANTHROPIC_URL),
                model_name: String::from("claude-3-haiku-20240307"),
                api_token: anthropic_token,
                parameters: None,
            })
        },
        ModelName::Claude3Sonnet => {
            let anthropic_token = env::var("ANTHROPIC_TOKEN").expect("Cannot find anthropic token. Please set ANTHROPIC_TOKEN env variable");
            ModelHandle::ANTHROPIC(AnthropicModelHandle {
                base_url: String::from(ANTHROPIC_URL),
                model_name: String::from("claude-3-sonnet-20240229"),
                api_token: anthropic_token,
                parameters: None,
            })
        },
        ModelName::Claude3Opus => {
            let anthropic_token = env::var("ANTHROPIC_TOKEN").expect("Cannot find anthropic token. Please set ANTHROPIC_TOKEN env variable");
            ModelHandle::ANTHROPIC(AnthropicModelHandle {
                base_url: String::from(ANTHROPIC_URL),
                model_name: String::from("claude-3-opus-20240229"),
                api_token: anthropic_token,
                parameters: None,
            })
        },
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use futures::future::join_all;

    #[tokio::test]
    async fn llm_correct_response() {
        let model_handle_1 = get_model_handle(ModelName::Gpt4);
        let model_handle_2 = get_model_handle(ModelName::Claude3Haiku);
        let model_handle_3 = get_model_handle(ModelName::Claude3Sonnet);
        let model_handle_4 = get_model_handle(ModelName::Claude3Opus);

        let user_prompt = "Write a short poem about rust the programming language.";
        let responses = join_all(vec![
            model_handle_1.get_response(user_prompt, None),
            model_handle_2.get_response(user_prompt, None),
            model_handle_3.get_response(user_prompt, None),
            model_handle_4.get_response(user_prompt, None),
        ])
        .await;

        let results: Result<String, APIError> = responses.into_iter().collect();

        assert!(results.is_ok());
    }
}
