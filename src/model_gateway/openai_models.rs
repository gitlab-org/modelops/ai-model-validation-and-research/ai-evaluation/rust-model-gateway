use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

use crate::model_gateway::APIError;

#[derive(Serialize, Deserialize, Debug)]
pub struct Payload {
    pub model: String,
    pub messages: Vec<HashMap<String, String>>,

    #[serde(flatten)]
    pub parameters: Value,
}

pub struct OpenAIModelHandle {
    pub base_url: String,
    pub model_name: String,
    pub api_token: String,
    pub parameters: Option<Value>,
}

impl OpenAIModelHandle {
    fn construct_payload(&self, prompt: &str, parameters: Option<Value>) -> Payload {
        let params = match parameters {
            Some(params) => params,
            None => json!({
                "temperature": 0.2,
                "max_tokens": 4096,
                "top_p": 0.5,
                "frequency_penalty": 0.0,
                "presence_penalty": 0.0,
            }),
        };

        Payload {
            model: self.model_name.to_owned(),
            messages: vec![
                HashMap::from([
                    (String::from("role"), String::from("system")),
                    (
                        String::from("content"),
                        String::from("You are a helpful assistant."),
                    ),
                ]),
                HashMap::from([
                    (String::from("role"), String::from("user")),
                    (String::from("content"), prompt.to_owned()),
                ]),
            ],
            parameters: params,
        }
    }

    async fn request(&self, prompt: &str, parameters: Option<Value>) -> Result<Value, APIError> {
        let payload = self.construct_payload(prompt, parameters);

        let client = reqwest::Client::new();
        let response = client
            .post(self.base_url.to_owned())
            .bearer_auth(self.api_token.to_owned())
            .json(&payload)
            .send()
            .await?;

        let status = response.status();
        let resp_json = response.json::<Value>().await?;

        if status.is_success() {
            Ok(resp_json)
        } else {
            Err(APIError::HTTPError(
                serde_json::to_string(&resp_json).unwrap(),
            ))
        }
    }

    pub async fn get_response(
        &self,
        prompt: &str,
        parameters: Option<Value>,
    ) -> Result<String, APIError> {
        let resp = self.request(prompt, parameters).await?;
        Ok(resp["choices"][0]["message"]["content"].to_string())
    }
}
